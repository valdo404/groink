
name := "groink"

version := "0.1"

scalaVersion := "2.12.10"

ThisBuild / useSuperShell := false
Global / onChangedBuildSource := ReloadOnSourceChanges

lazy val model = project in file("groink-model")
lazy val kafka = (project in file("groink-kafka")).dependsOn(model)
lazy val ingestion = (project in file("groink-ingestion")).dependsOn(kafka)
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)

lazy val alerts = (project in file("groink-alerts"))
  .dependsOn(kafka, ingestion)
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)

lazy val root = (project in file("."))
  .aggregate(model, kafka, ingestion, alerts)

