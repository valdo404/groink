This project is a reactive log agregator meant to be distributed

  - It takes a a textual source like a file
  
  - It parses it using a standardized log parser
  
  - It detects anomalies in the system

Some metrics are provided:
  
  - Statistics by url sections
  
  - Error rates by sections
  
  - Some statistics about http connections and sessions

If a file source is provided it is tailed on a continuous basis

When alerts are triggered, the alerts store receives alert messages
When alerts are back to normal, the alert store receives a "get back to normal" message

To build, first install sbt:

`brew install sbt`

And then:

`sbt test docker:publishLocal`

To run (via Docker):

`docker run --mount type=bind,source="$(pwd)"/tmp,target=/tmp alerts:0.1.0-SNAPSHOT --file /tmp/access.log`

`docker run alerts:0.1.0-SNAPSHOT` for available options

OR (via SBT)

`sbt "alerts/runMain org.levaldo.groink.EmbeddedFileAnalyzer --file /tmp/access.log"`

OR (via tar gz package)

`sbt universal:packageZipTarball` (everything will be in the bin directory)

Design ideas:

  * everything is reactive and back-pressured
  
  * design is stolen from druid (especially the segment idea)
  
  * avro schema is provided though vulcan module
  
  * reports and statistical segments are a semigroup.
  
  * there is the ingestion process and the reporting process, both of the two processes are totally concurrent.
  
  * limits are analyzed each second.

A kafka integration is also provided.
