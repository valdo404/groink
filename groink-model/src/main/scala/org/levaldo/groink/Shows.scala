package org.levaldo.groink

import java.time.Instant

import cats.Show
import org.levaldo.groink.AnalysisModel.{MonitoringEvent, SegmentStatistics}

object Shows {
  implicit val reportShow = Show.fromToString[SegmentStatistics]
  implicit val eventShow = Show.fromToString[MonitoringEvent]
  implicit val w3cLogShow = Show.fromToString[W3cLog]
  implicit val instantAndReportShow = Show.fromToString[(Instant, SegmentStatistics)]
}
