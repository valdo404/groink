package org.levaldo.groink

import java.time.Instant

import cats.Semigroup

object AnalysisModel {
  import cats.implicits._

  object SegmentStatistics {
    def empty: SegmentStatistics = SegmentStatistics(Map(), Map(), 0l, 0l)
  }

  case class SegmentStatistics(sections: Map[String, Long],
                               sectionErrors: Map[String, Long],
                               count: Long,
                               errors: Long)

  trait MonitoringEvent {
    val triggered: Boolean
  }
  case class TriggeredAlert(start: Instant, highWater: Long, currentValue: Double) extends MonitoringEvent {
    override val triggered = true
  }
  case class MutedAlert(start: Instant, highWater: Long, currentValue: Double) extends MonitoringEvent {
    override val triggered = false
  }

  implicit class ImprovedInt(status: Int) {
    def success = status.isStatusOf(200) || status.isStatusOf(300) || status.isStatusOf(100)

    def isStatusOf(category: Int) = {
      status - category < 100 && 0 <= status - category
    }
  }

  implicit val semiGroup = new Semigroup[SegmentStatistics] {
    override def combine(x: SegmentStatistics, y: SegmentStatistics): SegmentStatistics = SegmentStatistics(
      x.sections |+| y.sections,
      x.sectionErrors |+| y.sectionErrors,
      x.count |+| y.count,
      x.errors |+| y.errors
    )
  }


  implicit val instantAndReportSemigroup = new Semigroup[(Instant, SegmentStatistics)] {
    override def combine(x: (Instant, SegmentStatistics), y: (Instant, SegmentStatistics)): (Instant, SegmentStatistics) = {
      (
        Seq(x._1, y._1).min,
        x._2 |+| y._2
      )
    }
  }
}
