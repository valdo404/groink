package org.levaldo.groink

import java.time.{LocalTime, OffsetDateTime, ZoneOffset}

import com.google.common.base.MoreObjects

case class W3cLog(request: Option[String],
                  agent: Option[String],
                  auth: Option[String],
                  ident: Option[String],
                  verb: String,
                  time: OffsetDateTime,
                  referrer: Option[String],
                  response: Int,
                  bytes: Option[Long],
                  clientIp: String,
                  httpVersion: Option[String]) {
  override def toString: String = MoreObjects.toStringHelper(this)
    .add("request", request)
    .add("agent", agent)
    .add("auth", auth)
    .add("ident", ident)
    .add("verb", verb)
    .add("time", time)
    .add("referrer", referrer)
    .add("response", response)
    .add("bytes", bytes)
    .add("clientIp", clientIp)
    .add("httpVersion", httpVersion)
    .toString

  val sections = request.toList.flatMap(_.split("/").toList).drop(1)
  val firstPart = sections.dropRight(1).mkString("/", "/", "")
}


