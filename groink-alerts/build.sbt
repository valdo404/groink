scalacOptions += "-Ypartial-unification"

resolvers += "confluent" at "https://packages.confluent.io/maven/"

libraryDependencies += "com.github.benfradet" %% "gsheets4s" % "0.2.0"

libraryDependencies += "co.fs2" %% "fs2-io" % "2.0.1"
libraryDependencies += "co.fs2" %% "fs2-core" % "2.0.1"

libraryDependencies += "com.ovoenergy" %% "fs2-kafka" % "0.20.1"
libraryDependencies += "com.ovoenergy" %% "fs2-kafka-vulcan" % "0.20.1"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

libraryDependencies += "io.chrisdavenport" %% "cats-time" % "0.3.0"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.1"


libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

dockerEntrypoint := Seq("/opt/docker/bin/embedded-file-analyzer", "-J-server")
