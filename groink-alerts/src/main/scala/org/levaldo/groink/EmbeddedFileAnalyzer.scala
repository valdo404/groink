package org.levaldo.groink

import java.nio.file.Paths

import cats.effect.IOApp
import fs2.Pipe
import org.levaldo.groink.pipes.IngestionAndAnalysisProgram
import org.levaldo.groink.pipes.IngestionAndAnalysisProgram.ReportingConfig
import scopt.Zero

object EmbeddedFileAnalyzer extends IOApp {
  import cats.effect._

  import scala.concurrent.duration._

  case class FileStreamerConfig(file: String, reportingConfig: ReportingConfig)

  val parser = new scopt.OptionParser[FileStreamerConfig]("embedded-file-analyzer") {
    head("file-stream-analyzer", "0.1")

    opt[String]( "file").required()
      .action(
        (x, c) => c.copy(file = x) )

    opt[Duration]("reporting-delay").
      action(
        (x, c) => c.copy(reportingConfig = c.reportingConfig.copy(reportingDelay = x.asInstanceOf[FiniteDuration])) )

    opt[Duration]("alert-windowing").
      action(
        (x, c) => c.copy(reportingConfig = c.reportingConfig.copy(alertWindowing = x.asInstanceOf[FiniteDuration])) )

    opt[Int]("alert-trigger").
      action(
        (x, c) => c.copy(reportingConfig = c.reportingConfig.copy(alertLimit = x)) )
  }


  override def run(args: List[String]): IO[ExitCode] = {
    implicit val zero = Zero.zero(FileStreamerConfig("", ReportingConfig(10 seconds, 2 minutes, 10)))

    parser.parse(args, zero.zero) match {
      case Some(config) =>
        IngestionAndAnalysisProgram.run(config.reportingConfig)(fileSource(config.file))
      case None =>
        IO.pure(ExitCode.Error)
    }
  }

  private def fileSource(file: String)(analysisPipe: Pipe[IO, W3cLog, Unit]): IO[Unit] = {
    FileTailer.tailFile(Paths.get(file))
      .through(analysisPipe)
      .compile.drain
  }
}






