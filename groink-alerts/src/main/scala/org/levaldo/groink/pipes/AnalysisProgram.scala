package org.levaldo.groink.pipes

import java.io.PrintStream
import java.time.Instant

import cats.Parallel
import cats.effect.{Concurrent, IO, Timer}
import com.typesafe.scalalogging.LazyLogging
import fs2.Pipe
import org.levaldo.groink.persistence.{AlertRepository, SegmentRepository}
import org.levaldo.groink.pipes.IngestionAndAnalysisProgram.{ReportingConfig, logger}
import org.levaldo.groink.{AnalysisModel, Shows}

object AnalysisProgram extends LazyLogging {

  import AnalysisModel._
  import Shows._
  import cats.implicits._

  import scala.concurrent.duration._

  def launchAnalysis(config: ReportingConfig)
                    (reportingPipe: Pipe[IO, (Instant, SegmentStatistics), Unit])
                    (alertRepository: AlertRepository, reportRepository: SegmentRepository)
                    (implicit timer: Timer[IO], parallel: Parallel[IO], concurrent: Concurrent[IO]) = {
    List(
      awakeEvery(config.reportingDelay)
        .flatMap(_ =>
          reportRepository.last(config.reportingDelay).reduceSemigroup
        )
        .observe(reportingPipe)
        .compile.drain,
      awakeEvery(1 second)
        .flatMap(_ =>
          reportRepository.last(config.alertWindowing).reduceSemigroup
        )
        .through(AlertingPipe.alertWhen(config.alertWindowing, config.alertLimit))
        .observe(_.evalMap(alertRepository.storeMonitoringEvent))
        .evalTap{
          case t: TriggeredAlert => IO.delay(logger.error(s"High traffic generated an alert - hits = ${t.currentValue}, triggered at ${t.start}"))
          case t: MutedAlert => IO.delay(logger.info(s"Traffic got back to normal = ${t.currentValue}, at ${t.start}"))
        }
        .compile.drain)
      .parSequence
  }

  private def awakeEvery(finiteDuration: FiniteDuration)
                      (implicit timer: Timer[IO], concurrent: Concurrent[IO]): fs2.Stream[IO, FiniteDuration] = {
    fs2.Stream
      .awakeEvery[IO](finiteDuration)
  }
}
