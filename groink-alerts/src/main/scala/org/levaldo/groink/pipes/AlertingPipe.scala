package org.levaldo.groink.pipes

import java.time.Instant

import cats.Eq
import cats.effect.IO
import fs2.Pipe
import org.levaldo.groink.AnalysisModel.{MonitoringEvent, MutedAlert, SegmentStatistics, TriggeredAlert}

import scala.concurrent.duration.FiniteDuration

/** Computes a stream of alerts */
object AlertingPipe {
  import cats.implicits._

  private implicit val eventEq = Eq.by[MonitoringEvent, Boolean]{ _.triggered}

  def alertWhen(duration: FiniteDuration, limit: Long): Pipe[IO, (Instant, SegmentStatistics), MonitoringEvent] = {
    _.map { case (instant, report) => if (
      computeRate(duration, report) > limit)
      TriggeredAlert(instant, limit, computeRate(duration, report) )
    else
      MutedAlert(instant, limit, computeRate(duration, report))
    }
      .covaryOutput[MonitoringEvent]
      .changes
  }

  private def computeRate(duration: FiniteDuration, report: SegmentStatistics) = {
    report.errors / duration.toSeconds
  }
}
