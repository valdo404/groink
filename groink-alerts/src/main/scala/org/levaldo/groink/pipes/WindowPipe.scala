package org.levaldo.groink.pipes

import java.time.Instant

import cats.effect.IO
import fs2.Pipe
import org.levaldo.groink.AnalysisModel.SegmentStatistics

import scala.concurrent.duration.FiniteDuration

/** Streaming window on a arbitrary duration */
object WindowPipe {
  import cats.implicits._

  def windowOn(duration: FiniteDuration): Pipe[IO, (Instant, SegmentStatistics), (Instant, SegmentStatistics)] = {
    _
      .sliding(duration.toSeconds.toInt)
      .map(_.reduce(_ |+| _))
  }
}
