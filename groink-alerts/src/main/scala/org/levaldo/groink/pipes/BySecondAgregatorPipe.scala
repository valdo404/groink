package org.levaldo.groink.pipes

import java.time.Instant

import cats.effect.IO
import fs2.{Chunk, Pipe}
import org.levaldo.groink.{AnalysisModel, W3cLog}

/** Segment generator, in the spirit of Druid */
object BySecondAgregatorPipe {

  import AnalysisModel._
  import cats.implicits._
  import io.chrisdavenport.cats.time._

  def aggregateBySecond: Pipe[IO, W3cLog, (Instant, SegmentStatistics)] = {
    _
      .groupAdjacentBy(log => log.time.withNano(0).toInstant)
      .map { case (time, group) => {
        val erroneousTransactions = group.filter(!_.response.success)

        (time, SegmentStatistics(
          sections = aggregateSections(group.map(_.firstPart)),
          sectionErrors = aggregateSections(erroneousTransactions.map(_.firstPart)),
          count = group.size,
          errors = erroneousTransactions.size
        ))
      }}
  }


  private def aggregateSections(sections: Chunk[String]): Map[String, Long] = {
    sections.foldMap(section => Map(section -> 1))
  }
}
