package org.levaldo.groink.pipes

import java.time.Instant

import cats.Parallel
import com.typesafe.scalalogging.LazyLogging
import fs2.Pipe
import org.levaldo.groink.AnalysisModel.SegmentStatistics
import org.levaldo.groink.W3cLog
import org.levaldo.groink.persistence.{InMemoryAlertRepository, InMemorySegmentRepository}

import scala.concurrent.duration.FiniteDuration

object IngestionAndAnalysisProgram extends LazyLogging {
  import cats.effect._
  import cats.implicits._

  case class ReportingConfig(reportingDelay: FiniteDuration, alertWindowing: FiniteDuration, alertLimit: Long)

  type SourceProgram = Pipe[IO, W3cLog, Unit] => IO[Unit]

  def run(config: ReportingConfig)(sourceProgram: SourceProgram)(implicit timer: Timer[IO], parallel: Parallel[IO], concurrent: Concurrent[IO]): IO[ExitCode] = {
    val reportRepository = new InMemorySegmentRepository()
    val alertRepository = new InMemoryAlertRepository()
    val reportingPipe: Pipe[IO, (Instant, SegmentStatistics), Unit] = _.evalTap(report => IO.delay(logger.info(s"New report $report"))).drain

    val analysisProcess: IO[Unit] = AnalysisProgram.launchAnalysis(config)(reportingPipe)(alertRepository, reportRepository).void
    val ingestionProcess: IO[Unit] = sourceProgram(ingestionPipe(reportRepository))

    List(ingestionProcess, analysisProcess)
      .parSequence
      .as(ExitCode.Success)
  }


  private def ingestionPipe(reportRepository: InMemorySegmentRepository)
                   (implicit concurrent: Concurrent[IO], timer: Timer[IO], parallel: Parallel[IO]): fs2.Pipe[IO, W3cLog, Unit] = {
    _
      .evalTap(log => IO.delay(logger.debug(s"Incoming log $log")))
      .through(BySecondAgregatorPipe.aggregateBySecond)
      .map(_._2)
      .evalTap(report => IO.delay(logger.debug(s"New stored report $report")))
      .mapAsync(5)(reportRepository.storeSegment(Instant.now(), _))
  }
}
