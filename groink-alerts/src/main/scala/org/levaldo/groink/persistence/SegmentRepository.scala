package org.levaldo.groink.persistence

import java.time.Instant
import java.time.temporal.ChronoUnit

import cats.effect.IO
import org.levaldo.groink.AnalysisModel.SegmentStatistics

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration

trait SegmentRepository {
  def storeSegment(insertion: Instant, segment: SegmentStatistics): IO[Unit]

  def byInsertion(from: Instant, to: Instant): fs2.Stream[IO, (Instant, SegmentStatistics)]

  def last(duration: FiniteDuration): fs2.Stream[IO, (Instant, SegmentStatistics)] = {
    val instant = Instant.now()
    byInsertion(instant.minusSeconds(duration.toSeconds), instant)
  }
}


class InMemorySegmentRepository extends SegmentRepository {

  import io.chrisdavenport.cats.time._

  private val byInsertionMap: mutable.TreeMap[Instant, SegmentStatistics] = mutable.TreeMap
    .empty[Instant, SegmentStatistics](instantInstances.toOrdering)

  override def storeSegment(instant: Instant, segment: SegmentStatistics): IO[Unit] = {
    IO.delay(byInsertionMap += ((instant, segment)))
  }

  override def byInsertion(from: Instant, to: Instant): fs2.Stream[IO, (Instant, SegmentStatistics)] = {
    fs2.Stream.emits(fillTheHoles(from, to,
      byInsertionMap.range(from, to.plusSeconds(1))).toSeq)
  }


  private def fillTheHoles(from: Instant, to: Instant, map: mutable.SortedMap[Instant, SegmentStatistics]): mutable.SortedMap[Instant, SegmentStatistics] = {

    val newMap = map.clone()

    (1l to from.until(to, ChronoUnit.SECONDS))
      .foldLeft(newMap) {
        case (currentMap, delta) => {
          val instant = from.plusSeconds(delta)

          if (!currentMap.contains(instant))
            currentMap.updated(instant, SegmentStatistics.empty)
          else
            currentMap
        }
      }
  }
}