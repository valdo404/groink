package org.levaldo.groink.persistence

import cats.effect.IO
import org.levaldo.groink.AnalysisModel.MonitoringEvent

import scala.collection.mutable

trait AlertRepository {
  def storeMonitoringEvent(event: MonitoringEvent): IO[Unit]
  def fetchAll: fs2.Stream[IO, MonitoringEvent]
}


class InMemoryAlertRepository() extends AlertRepository {
  val queue: mutable.Queue[MonitoringEvent] = mutable.Queue.newBuilder[MonitoringEvent].result()
  override def storeMonitoringEvent(event: MonitoringEvent): IO[Unit] = {
    IO.delay(queue.enqueue())
  }

  override def fetchAll: fs2.Stream[IO, MonitoringEvent] = {
    fs2.Stream.emits(queue.toList)
  }
}