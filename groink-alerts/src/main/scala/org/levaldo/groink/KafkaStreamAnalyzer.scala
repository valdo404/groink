package org.levaldo.groink

import cats.effect.IOApp
import fs2.Pipe
import fs2.kafka.vulcan.{AvroSettings, SchemaRegistryClientSettings, avroDeserializer}
import org.levaldo.groink.pipes.IngestionAndAnalysisProgram
import org.levaldo.groink.pipes.IngestionAndAnalysisProgram.ReportingConfig

/**
  * Program that do reporting and alerting on a stream of W3cLogs
  */
object KafkaStreamAnalyzer extends IOApp {
  import AvroCodecs._
  import cats.effect._
  import fs2.kafka._

  import scala.concurrent.duration._
  import scopt._

  case class KafkaConfig(schemaRegistryUrl: String = "http://localhost:6002",
                         kafkaBootstrap: String = "localhost:6001",
                         groupId: String = "analyzer",
                         topic: String = "w3c")

  case class KafkaStreamerConfig(kafkaConfig: KafkaConfig, reportingConfig: ReportingConfig)

  val parser = new scopt.OptionParser[KafkaStreamerConfig]("kafka-stream-analyzer") {
    head("kafka-stream-analyzer", "0.1")

    opt[String]( "schema-registry").required()
      .action(
        (x, c) => c.copy(kafkaConfig = c.kafkaConfig.copy(schemaRegistryUrl = x)) )

    opt[String]("kafka-bootstrap").required().
      action(
      (x, c) => c.copy(kafkaConfig = c.kafkaConfig.copy(kafkaBootstrap = x)) )

    opt[String]("group-id").required().
      action(
        (x, c) => c.copy(kafkaConfig = c.kafkaConfig.copy(groupId = x)) )

    opt[String]("topic").required().
      action(
        (x, c) => c.copy(kafkaConfig = c.kafkaConfig.copy(topic = x)) )

    opt[Duration]("reporting-delay").
      action(
        (x, c) => c.copy(reportingConfig = c.reportingConfig.copy(reportingDelay = x.asInstanceOf[FiniteDuration])) )

    opt[Duration]("alert-windowing").
      action(
        (x, c) => c.copy(reportingConfig = c.reportingConfig.copy(alertWindowing = x.asInstanceOf[FiniteDuration])) )

    opt[Int]("alert-trigger").
      action(
        (x, c) => c.copy(reportingConfig = c.reportingConfig.copy(alertLimit = x)) )

  }

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val zero = Zero.zero(KafkaStreamerConfig(KafkaConfig(), ReportingConfig(10 seconds, 2 minutes, 10)))

    parser.parse(args, zero.zero) match {
      case Some(config) =>
        IngestionAndAnalysisProgram.run(config.reportingConfig)(kafkaSource(config.kafkaConfig))
      case None =>
        IO.pure(ExitCode.Error)
    }
  }


  /** Provide a streaming source for kafka */
  private def kafkaSource(kafkaConfig: KafkaConfig)(analysisPipe: Pipe[IO, W3cLog, Unit]): IO[Unit] = {
    val avroSettings =
      AvroSettings {
        SchemaRegistryClientSettings[IO](kafkaConfig.schemaRegistryUrl)
      }

    implicit val logDeserializer: Deserializer.Record[IO, W3cLog] =
      avroDeserializer[W3cLog].using(avroSettings)

    val consumerSettings =
      ConsumerSettings[IO, Unit, W3cLog]
        .withAutoOffsetReset(AutoOffsetReset.Earliest)
        .withBootstrapServers(kafkaConfig.kafkaBootstrap)
        .withGroupId(kafkaConfig.groupId)


    val segmentsFromKafka: Pipe[IO, CommittableConsumerRecord[IO, Unit, W3cLog], Unit] =
      analysisPipe
        .compose(_.map(_.record.value))

    consumerStream[IO]
      .using(consumerSettings)
      .evalTap(_.subscribeTo(kafkaConfig.topic))
      .flatMap(_.stream)
      .observe(segmentsFromKafka)
      .map(_.offset)
      .through(commitBatchWithin[IO](500, 15.seconds))
      .compile.drain
  }


}






