package org.levaldo.groink

import java.time.Instant

import cats.effect.{ContextShift, IO, Timer}
import org.levaldo.groink.AnalysisModel.SegmentStatistics
import org.levaldo.groink.persistence.InMemorySegmentRepository
import org.scalatest.FunSuite

import scala.concurrent.ExecutionContext

class InMemorySegmentStatisticsRepositoryTest extends FunSuite  {
  import Shows._
  import cats.implicits._

  implicit val executionContext =
    ExecutionContext.global
  implicit val timer: Timer[IO] =
    IO.timer(executionContext)
  implicit val cs: ContextShift[IO] =
    IO.contextShift(executionContext)
  implicit val parallel = IO.ioParallel(cs)

  test("test store report returns good results") {
    val store = new InMemorySegmentRepository()
    val instant = Instant.now()
    val instantPlus1 = instant.plusSeconds(5)

    (store.storeSegment(instant, SegmentStatistics(Map("/" -> 1), Map(), 0, 0)) *>
      store.storeSegment(instantPlus1, SegmentStatistics(Map("/" -> 1, "/api" -> 2), Map("/" -> 2), 0, 0))).unsafeRunSync()

    val source = store
      .byInsertion(instantPlus1.minusSeconds(30), instantPlus1)

    val reports = source.compile.toList.unsafeRunSync()
    assert(reports.size == 30)

    val tuples: fs2.Stream[IO, (Instant, SegmentStatistics)] = source
      .observe(_.showLines(Console.out))
      .reduceSemigroup

    val finalReports = tuples.compile.toList.unsafeRunSync()
    assert(finalReports.size == 1)
  }

  test("test it is also working with empty results") {
    val store = new InMemorySegmentRepository()
    val instant = Instant.now()
    val instantPlus1 = instant.plusSeconds(3)

    val source: fs2.Stream[IO, (Instant, SegmentStatistics)] = store
      .byInsertion(instantPlus1.minusSeconds(10), instantPlus1)

    val reports = source.compile.toList.unsafeRunSync()
    assert(reports.size == 10)

    val tuples: fs2.Stream[IO, (Instant, SegmentStatistics)] = source
      .reduceSemigroup

    val finalReports = tuples.compile.toList.unsafeRunSync()
    assert(finalReports.size == 1)

  }



}
