package org.levaldo.groink

import java.time.{Instant, LocalDateTime, OffsetDateTime, ZoneOffset}

import org.levaldo.groink.AnalysisModel.TriggeredAlert
import org.levaldo.groink.pipes.{AlertingPipe, BySecondAgregatorPipe, WindowPipe}
import org.scalatest.{FunSuite, GivenWhenThen, Matchers}

import scala.concurrent.duration._

class StreamAnalyserTest extends FunSuite with Matchers with GivenWhenThen {
  val logsAtStart = W3cLog(Some("/tmp2/test.php"), None, None, None, "GET", OffsetDateTime
    .of(2019, 10, 10, 0, 0, 1, 0, ZoneOffset.ofHours(0)), None, 200, None, "127.0.0.1", None)
  val oneMinuteLater = W3cLog(Some("/tmp2/test.php"), None, None, None, "GET", OffsetDateTime
    .of(2019, 10, 10, 0, 1, 3, 0, ZoneOffset.ofHours(0)), None, 200, None, "127.0.0.1", None)
  val erroneousLog = W3cLog(Some("/tmp2/test.php"), None, None, None, "GET", OffsetDateTime
    .of(2019, 10, 10, 0, 1, 3, 0, ZoneOffset.ofHours(0)), None, 500, None, "127.0.0.1", None)
  val threeMinutesLater = W3cLog(Some("/tmp2/test.php"), None, None, None, "GET", OffsetDateTime
    .of(2019, 10, 10, 0, 3, 3, 0, ZoneOffset.ofHours(0)), None, 200, None, "127.0.0.1", None)
  val fiveMinutesLater = W3cLog(Some("/tmp2/test.php"), None, None, None, "GET", OffsetDateTime
    .of(2019, 10, 10, 0, 5, 3, 0, ZoneOffset.ofHours(0)), None, 200, None, "127.0.0.1", None)

  test ("We create segments with granularity of one second, with possible holes in consideration") {

    val logFixtures =
      Seq.fill(3)(logsAtStart) ++ Seq.fill(2)(oneMinuteLater) ++ Seq.fill(2000)(erroneousLog) ++ Seq.fill(1)(threeMinutesLater) ++ Seq.fill(1)(fiveMinutesLater)

    val alertingCycle = fs2.Stream.emits(logFixtures)
      .through(BySecondAgregatorPipe.aggregateBySecond)
      .through(WindowPipe.windowOn(duration = 2 minutes))
      .through(AlertingPipe.alertWhen(limit = 10l, duration = 2 minutes))
      .compile.toList.unsafeRunSync()

    println(alertingCycle)
    assert(alertingCycle == List(TriggeredAlert(
      LocalDateTime.of(2019, 10, 10, 0, 0, 1)
        .toInstant(ZoneOffset.UTC), 10, 16.0)))
  }

  test ("windowed aggregation") {
    val logFixtures =
      Seq.fill(3)(logsAtStart) ++ Seq.fill(1)(oneMinuteLater) ++ Seq.fill(1)(erroneousLog)

    val bySecondAggregated = fs2.Stream.emits(logFixtures)
      .through(BySecondAgregatorPipe.aggregateBySecond)
    val segments = bySecondAggregated
      .compile.toList.unsafeRunSync()

    println(segments)
    assert(segments.size == 2) // holes should be filled, which gives 63 segments, on a timeframe of 63 seconds
  }

  test("report generated each N seconds") {
    val logFixtures =
      Seq.fill(3)(logsAtStart) ++ Seq.fill(1)(oneMinuteLater) ++ Seq.fill(1)(erroneousLog) ++ Seq.fill(1)(threeMinutesLater) ++ Seq.fill(1)(fiveMinutesLater)

      val onTenSeconds = fs2.Stream.emits(logFixtures)
        .through(BySecondAgregatorPipe.aggregateBySecond)
        .compile.toList.unsafeRunSync()

    println(onTenSeconds)
    assert(onTenSeconds.size == 4) // there is 61 generated reports in a delay of 63 seconds
  }

  test ("Erroneous logs should be detected accordingly") {
    import AnalysisModel._

    assert(100.success === true)
    assert(200.success === true)
    assert(500.success === false)
    assert(400.success === false)
  }
}
