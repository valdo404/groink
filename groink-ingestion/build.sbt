resolvers += "confluent" at "https://packages.confluent.io/maven/"

libraryDependencies += "io.krakens" % "java-grok" % "0.1.9"
libraryDependencies += "co.fs2" %% "fs2-io" % "2.0.1"
libraryDependencies += "co.fs2" %% "fs2-core" % "2.0.1"

libraryDependencies += "com.ovoenergy" %% "fs2-kafka" % "0.20.1"
libraryDependencies += "com.ovoenergy" %% "fs2-kafka-vulcan" % "0.20.1"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.1"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

