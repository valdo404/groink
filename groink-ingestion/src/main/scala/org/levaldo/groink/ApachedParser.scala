package org.levaldo.groink

import java.time.format.DateTimeFormatterBuilder
import java.time.{OffsetDateTime, YearMonth, ZoneOffset}
import java.util.Locale

import io.krakens.grok.api.{Grok, GrokCompiler}

import scala.collection.mutable

object ApachedParser {
  import scala.collection.JavaConverters._

  private val grokCompiler: GrokCompiler = GrokCompiler.newInstance
  grokCompiler.registerDefaultPatterns()

  val apacheParser: Grok = grokCompiler.compile("%{COMBINEDAPACHELOG}")

  private val yearMonthFormatter = new DateTimeFormatterBuilder()
    .parseCaseInsensitive()
    .appendPattern("MMM yyyy")
    .toFormatter(new Locale("en", "UK"))

  def toLogLine(rawLog: String): Option[W3cLog] = {
    val gm = ApachedParser.apacheParser.`match`(rawLog)
    val captured = gm.capture.asScala

    Some(W3cLog(
      request = Option(captured("request")).map(_.toString),
      agent = Option(captured("agent")).map(_.toString),
      auth = Option(captured("auth")).filter(_ != "-").map(_.toString),
      ident = Option(captured("ident")).filter(_ != "-").map(_.toString),
      verb = captured("verb").toString,
      time = toTime(captured),
      referrer = Option(captured("referrer")).filter(_ != "-").map(_.toString),
      response = captured("response").toString.toInt,
      bytes = Option(captured("bytes")).map(_.toString.toLong),
      clientIp = captured("clientip").toString,
      httpVersion = Option(captured("httpversion")).map(_.toString)
    ))
  }

  private def toTime(captured: mutable.Map[String, AnyRef]) = {
    val yearMonth = YearMonth.parse(captured("MONTH").toString + " " + captured("YEAR").toString, yearMonthFormatter)

    OffsetDateTime.of(yearMonth.getYear,
      yearMonth.getMonthValue,
      captured("MONTHDAY").toString.toInt, captured("HOUR").toString.toInt,
      captured("MINUTE").toString.toInt, captured("SECOND").toString.toInt, 0, ZoneOffset
        .of(captured("INT").toString))
  }
}
