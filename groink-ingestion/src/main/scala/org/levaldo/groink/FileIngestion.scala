package org.levaldo.groink

import java.nio.file.{Path, Paths}

import cats.effect.{Blocker, ContextShift, ExitCode, IO, IOApp, Timer}
import fs2.Pipe
import fs2.io.file
import fs2.kafka.vulcan.{AvroSettings, SchemaRegistryClientSettings}
import org.levaldo.groink.FileIngestion.blocker
import scopt.Zero

import scala.concurrent.ExecutionContext

object FileIngestion extends IOApp {
  import AvroCodecs._
  import Shows._
  import cats.implicits._
  import fs2.kafka._
  import fs2.kafka.vulcan.avroSerializer

  val blocker = Blocker.liftExecutionContext(ExecutionContext.global)

  case class KafkaConfig(schemaRegistryUrl: String = "http://localhost:6002",
                         kafkaBootstrap: String = "localhost:6001",
                         topic: String = "w3c")
  case class IngestionConfig(kafkaConfig: KafkaConfig, file: String)


  val parser = new scopt.OptionParser[IngestionConfig]("kafka-stream-analyzer") {
    head("kafka-stream-analyzer", "0.1")

    opt[String]( "schema-registry").required()
      .action(
        (x, c) => c.copy(kafkaConfig = c.kafkaConfig.copy(schemaRegistryUrl = x)) )

    opt[String]("kafka-bootstrap").required().
      action(
        (x, c) => c.copy(kafkaConfig = c.kafkaConfig.copy(kafkaBootstrap = x)) )

    opt[String]("topic").required().
      action(
        (x, c) => c.copy(kafkaConfig = c.kafkaConfig.copy(topic = x)) )

    opt[String]("file").required().
      action(
        (x, c) => c.copy(file = x))
  }

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val zero = Zero.zero(IngestionConfig(KafkaConfig(), ""))

    parser.parse(args, zero.zero) match {
      case Some(config) =>
        launch(config)
      case None =>
        IO.pure(ExitCode.Error)
    }
  }

  private def launch(ingestionConfig: IngestionConfig) = {
    val avroSettings =
      AvroSettings {
        SchemaRegistryClientSettings[IO](ingestionConfig.kafkaConfig.schemaRegistryUrl)
      }

    implicit val logSerializer: Serializer.Record[IO, W3cLog] =
      avroSerializer[W3cLog].using(avroSettings)

    val producerSettings: ProducerSettings[IO, Unit, W3cLog] =
      ProducerSettings[IO, Unit, W3cLog]
        .withBootstrapServers(ingestionConfig.kafkaConfig.kafkaBootstrap)

    val kafkaPipe: Pipe[IO, ProducerRecords[Unit, W3cLog, Unit], ProducerResult[Unit, W3cLog, Unit]] = produce(producerSettings)
    val lines: Pipe[IO, W3cLog, Unit] = _.showLines(Console.out)

    FileTailer.tailFile(Paths.get(ingestionConfig.file))
      .observe(lines)
      .map(record => {
        ProducerRecords.one(ProducerRecord(ingestionConfig.kafkaConfig.topic, (), record))
      })
      .through(kafkaPipe)
      .compile
      .drain
      .as(ExitCode.Success)
  }
}

object FileTailer {
  def tailFile(filePath: Path)(implicit contextShift: ContextShift[IO], timer: Timer[IO]): fs2.Stream[IO, W3cLog] = {
    file.tail[IO](filePath, blocker, 1000)
      .through(fs2.text.utf8Decode)
      .through(fs2.text.lines)
      .map(W3cParser.toLogLine)
      .collect{ case Some(log) => log }
  }
}