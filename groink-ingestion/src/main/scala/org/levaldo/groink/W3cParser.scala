package org.levaldo.groink

import java.time.format.DateTimeFormatterBuilder
import java.time.{OffsetDateTime, YearMonth, ZoneOffset}
import java.util.Locale

object W3cParser {
  val w3c = """(.+) (.+) (.+) \[(.+)\] "(.+) (.*) (HTTP.+)" (\d+) (\d+)""".r

  private val yearMonthFormatter = new DateTimeFormatterBuilder()
    .parseCaseInsensitive()
    .appendPattern("d/MMMM/y:H:m:s")
    .appendLiteral(" ")
    .appendPattern("xxxx")
    .toFormatter(new Locale("en", "UK"))

  implicit class StringOps(str: Option[String]) {
    def removeDash = str.filter(_ != "-")
  }


  def toLogLine(rawLog: String): Option[W3cLog] = {
    w3c.findFirstMatchIn(rawLog).map(mat => {
      W3cLog(
        request = Option(mat.group(6)).removeDash,
        agent = None,
        auth = Option(mat.group(3)),
        ident = None,
        verb = mat.group(5),
        time = toTime(mat.group(4)),
        referrer = None,
        response = mat.group(8).toInt,
        bytes = Option(mat.group(9)).removeDash.map(_.toLong),
        clientIp = mat.group(1),
        httpVersion = Option(mat.group(7)).removeDash
      )
    })
  }

  private def toTime(date: String) = {
    OffsetDateTime.parse(date, yearMonthFormatter)
  }
}
