package org.levaldo.groink

import java.time.{OffsetDateTime, ZoneOffset}

import org.scalatest.{FunSuite, GivenWhenThen, Matchers}

import scala.io.Source

class GroinkIngestionTest extends FunSuite with
  Matchers with
  GivenWhenThen {
  test("We can parse a apache httpd log file") {
    Given("a log file from access")
    val lines: Iterator[W3cLog] = Source.fromResource("access.short.log").getLines()
      .flatMap(ApachedParser.toLogLine)

    Given("a kafka cluster")
    Then("we are able to grok the the log file")
    Then("it is sent to kafka")
    println(lines.toList.take(100))
  }

  test("a log line can be parsed") {
    Given("a log line from httpd server")
    val log = "89.178.243.193 - - [13/Dec/2015:14:37:16 +0100] \"POST /administrator/index.php HTTP/1.1\" 200 4494 \"http://almhuette-raith.at/administrator/\" \"Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0\" \"-\""

    val line = ApachedParser.toLogLine(log)

    Then("we are able to grok the log line")
    assert(line.exists(_.request.contains("/administrator/index.php")))
    assert(line.exists(_.auth.isEmpty))
    assert(line.exists(_.time == OffsetDateTime.of(2015, 12, 13, 14, 37, 16, 0, ZoneOffset.ofHours(1))))
  }

  test("We can parse a w3c httpd log lines") {
    Given("a log file from access")
    val lines: Iterator[W3cLog] = Source.fromResource("exercise.log").getLines()
      .flatMap(W3cParser.toLogLine)

    Given("a kafka cluster")
    Then("we are able to grok the the log file")
    Then("it is sent to kafka")
    val head = lines.toList.head
    assert(head.ident.isEmpty)
    assert(head.auth.contains("james"))
  }
}
