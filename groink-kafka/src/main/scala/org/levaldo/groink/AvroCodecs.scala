package org.levaldo.groink

import java.time.{LocalTime, OffsetDateTime, ZoneOffset}

object AvroCodecs {
  import cats.implicits._
  import vulcan._
  import vulcan.generic._

  implicit def odtCodec: Codec[OffsetDateTime] = Codec.record[OffsetDateTime](
    name = "OffsetDateTime",

  ) { field: Codec.FieldBuilder[OffsetDateTime] =>
    (
      field("date", _.toLocalDate),
      field("time", _.toLocalTime),
      field("offset", _.getOffset)
    ).mapN(OffsetDateTime.of)
  }

  implicit def ltCodec: Codec[LocalTime] = Codec.long.imap(LocalTime.ofSecondOfDay)(_.toSecondOfDay)
  implicit def zoCodec: Codec[ZoneOffset] = Codec.int.imap(ZoneOffset.ofTotalSeconds)(_.getTotalSeconds)
  implicit def w3logCodec: Codec[W3cLog] = Codec.derive[W3cLog]
}
